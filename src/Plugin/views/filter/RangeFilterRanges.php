<?php

namespace Drupal\range_filter_ranges\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Range views range filter.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("range_filter_ranges")
 */
class RangeFilterRanges extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  protected $alwaysMultiple = TRUE;

  /**
   * {@inheritdoc}
   */
  public $operator = 'contains';

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['operator'] = ['default' => 'contains'];

    $options['value'] = [
      'contains' => [
        'min' => ['default' => ''],
        'max' => ['default' => '']
      ],
    ];

    $options['include_endpoints'] = ['default' => FALSE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function operatorOptions($which = 'title') {
    $options = [];
    foreach ($this->operators() as $id => $value) {
      $options[$id] = $value[$which];
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $form['value']['#tree'] = TRUE;
    $form['value']['min'] = [
      '#type' => 'number',
      '#title' => $this->t('Min'),
      '#size' => 30,
      '#default_value' => $this->value['min'],
    ];
    $form['value']['max'] = [
      '#type' => 'number',
      '#title' => $this->t('Max'),
      '#size' => 30,
      '#default_value' => $this->value['max'],
    ];
    if (!$form_state->get('exposed')) {
      $form['include_endpoints'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Include endpoints'),
        '#default_value' => $this->options['include_endpoints'],
        '#description' => $this->t('Whether to include endpoints or not.'),
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    $field_from = "$this->tableAlias.{$this->definition['additional fields']['from']}";
    $field_to = "$this->tableAlias.{$this->definition['additional fields']['to']}";

    $operators = $this->operators();
    if (!empty($operators[$this->operator]['method'])) {
      $this->{$operators[$this->operator]['method']}($field_from, $field_to);
    }
  }

  /**
   * Operator callback.
   */
  protected function opContains($field_from, $field_to) {
    $operators = [
      '<', '>',
      '<=', '>=',
    ];

    $include_endpoints = $this->options['include_endpoints'];
    list($op_left, $op_right) = array_slice($operators, $include_endpoints ? 2 : 0, 2);

    $condition = $this->query->getConnection()->condition('AND');
    $empty = true;
    if (!empty($this->value['max'])) {
      $condition = $condition->condition($field_from, $this->value['max'], $op_left);
      $empty = false;
    }
    if (!empty($this->value['min'])) {
      $condition = $condition->condition($field_to, $this->value['min'], $op_right);
      $empty = false;
    }
    if (!$empty) {
      $this->query->addWhere($this->options['group'], $condition);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    if ($this->isAGroup()) {
      return $this->t('grouped');
    }
    if (!empty($this->options['exposed'])) {
      return $this->t('exposed');
    }

    return $this->value;
  }

  /**
   * Define the operators supported for ranges.
   */
  protected function operators() {
    $operators = [
      'contains' => [
        'title' => $this->t('Range contains'),
        'short' => $this->t('contains'),
        'method' => 'opContains',
        'values' => 2,
      ],
    ];

    return $operators;
  }

}
