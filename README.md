# Range Filter Ranges

This module provides a views filter for the Range module's range_integer field.

By default the filter provided by the Range module can be used to search for
a single number inside the ranges.

The filter implemented in this module however provides a min and a max that
can be used to find ranges that are overlapping with the range defined by
the given values.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/range_filter_ranges).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/range_filter_ranges).


## Requirements

This module requires the following module:

- [Range](https://www.drupal.org/project/range)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

This module does not have configuration.


## Maintainers

- Bálint Nagy - [nagy.balint](https://www.drupal.org/u/nagybalint)
