<?php

/**
 * @file
 * Provides views data for the range_filter module.
 */

 
use Drupal\Core\Entity\ContentEntityInterface;

function getFieldsByType(string $type, string $entity_type): array {
$properties = ['type' => $type, 'entity_type' => $entity_type];
  $entityTypeManager = \Drupal::entityTypeManager();
  $fields = $entityTypeManager
    ->getStorage('field_storage_config')
    ->loadByProperties($properties);
  return $fields;
}

/**
 * Implements hook_views_data_alter().
 */
function range_filter_ranges_views_data_alter(array &$data) {
  // Provides an integration for each entity type.
  foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type_id => $entity_type) {
    if (!$entity_type->entityClassImplements(ContentEntityInterface::class) || !$entity_type->getBaseTable()) {
      continue;
    }
    
    $int_fields = getFieldsByType('range_integer', $entity_type_id);
    $float_fields = getFieldsByType('range_float', $entity_type_id);

    $field_map = array_merge($int_fields, $float_fields);

    foreach ($field_map as $field_id => $field) {
      $field_name = $field->getName();

      $label = views_entity_field_label($entity_type_id, $field_name)[0];

      $storage = _views_field_get_entity_type_storage($field);
      $table_mapping = $storage->getTableMapping();
      $add_field_names = ['delta', 'langcode', 'bundle'];
      $add_fields = array_combine($add_field_names, $add_field_names);
      foreach (array_keys($field->getColumns()) as $column) {
        $add_fields[$column] = $table_mapping->getFieldColumnName($field, $column);
      }
      $data_table = $table_mapping->getDedicatedDataTableName($field);

      $data[$data_table][$field_name . '_filter_ranges'] = array(
        'group' => t('Content'),
        'filter' => array(
          'table' => $data_table,
          'field_name' => $field_name,
          'title' => t('@label range filter on ranges', ['@label' => $label]),
          'help' => t('Range filter on ranges.'),
          'field' => $field_name,
          'id' => 'range_filter_ranges',
          'additional fields' => $add_fields
        ),
      );
    }
  }
}
